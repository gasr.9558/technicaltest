package com.example.technicaltest

import androidx.multidex.MultiDexApplication
import io.realm.Realm
import io.realm.RealmConfiguration

class BaseApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
        val configuration: RealmConfiguration = RealmConfiguration.Builder()
            .schemaVersion(2)
            .name("db.technicalTest")
            .deleteRealmIfMigrationNeeded()
            .build()

        Realm.getInstance(configuration)
        Realm.setDefaultConfiguration(configuration)
    }
}