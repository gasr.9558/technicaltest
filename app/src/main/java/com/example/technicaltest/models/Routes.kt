package com.example.technicaltest.models

import com.google.android.gms.maps.model.LatLng
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.io.Serializable

open class Routes: RealmObject(), Serializable {

    @PrimaryKey
    open var id: Int = -1
    open var name: String = ""
    open var distance: String = ""
    open var timeLapse: String = ""
    open var lisPoints: RealmList<String> = RealmList()

    companion object {
        fun create(id: Int, name: String, distance: String, timeLapse: String, listPoints: RealmList<String>): Routes {
            val r = Routes()

            r.id = id
            r.name = name
            r.distance = distance
            r.timeLapse = timeLapse
            r.lisPoints = listPoints

            return r
        }
    }

}