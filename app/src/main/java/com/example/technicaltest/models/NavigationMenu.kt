package com.example.technicaltest.models

import android.content.Context
import androidx.fragment.app.Fragment
import com.example.technicaltest.Fragments.FragmentRecord
import com.example.technicaltest.Fragments.FragmentRoute
import com.example.technicaltest.R

enum class NavigationMenu {

    ROUTE, RECORDS;

    fun getFragment(): Fragment = when (this) {
        ROUTE -> {
            FragmentRoute.getInstance()
        }
        RECORDS -> {
            FragmentRecord.getIntance()
        }
    }

    fun getTitle(ctx: Context): String = when (this) {
        ROUTE -> {
            ctx.getString(R.string.route)
        }
        RECORDS -> {
            ctx.getString(R.string.record)
        }
    }
}