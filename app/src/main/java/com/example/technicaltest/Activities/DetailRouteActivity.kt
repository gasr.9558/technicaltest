package com.example.technicaltest.Activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.example.technicaltest.R
import com.example.technicaltest.models.Routes
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_detail_routes.*
import kotlin.math.roundToInt

class DetailRouteActivity: AppCompatActivity(), OnMapReadyCallback {

    private var route: Routes? = null
    private val realm: Realm = Realm.getDefaultInstance()
    private lateinit var mMap: GoogleMap
    //private var pointList: List<LatLng> = arrayListOf()
    private var listAux = arrayListOf<LatLng>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_routes)

        val id = this.intent.getIntExtra("id_route", 0)

        route = realm.where(Routes::class.java).equalTo("id", id).findFirst()

        txt_name_detail.text = route!!.name
        val dis = "${route!!.distance.toDouble().roundToInt()} metros"
        txt_distance_detail.text = dis

        val seg = "${route!!.timeLapse.toDouble() / 1000} seg"
        txt_time_detail.text = seg
        //pointList = route!!.lisPoints



        for (i in route!!.lisPoints) {
            val parts = i.split(",")
            var lat = parts[0]
            var lng = parts[1]
            var ll = LatLng(lat.toDouble(), lng.toDouble())

            listAux.add(ll)
        }

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map_route_detail) as SupportMapFragment
        mapFragment.getMapAsync(this)

        btn_delete.setOnClickListener{

            realm.executeTransactionAsync {
                val item = it.where(Routes::class.java).equalTo("id", id).findFirst()
                item!!.deleteFromRealm()
            }
        }

        btn_share.setOnClickListener {
            val shareBody = "Mira mi nuevo record en ruta, Tiempo: ${route!!.timeLapse.toDouble() / 1000} seg" +
                    "\nDistancia: ${route!!.distance.toDouble().roundToInt()} metros"
            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here")
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
            startActivity(Intent.createChooser(sharingIntent, resources.getString(R.string.share)))
        }

    }

    override fun onMapReady(p0: GoogleMap) {

        mMap = p0

        val pointInit = listAux[0]
        mMap.addMarker(MarkerOptions().position(pointInit)
            .title("Inicio"))
            .setIcon(BitmapDescriptorFactory.fromResource(R.drawable.init))

        val pointFinal = listAux[listAux.size - 1]

        mMap.addMarker(MarkerOptions().position(pointFinal)
            .title("Final"))
            .setIcon(BitmapDescriptorFactory.fromResource(R.drawable.flag))

        var line = PolylineOptions()

        for (item in listAux) {
            line.add(item)
        }

        mMap.addPolyline(line)

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pointInit, 15f))
    }
}