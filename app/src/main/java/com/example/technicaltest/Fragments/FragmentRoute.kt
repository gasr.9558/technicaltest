package com.example.technicaltest.Fragments

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.example.technicaltest.R
import com.example.technicaltest.models.Routes
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.SphericalUtil
import io.realm.Realm
import io.realm.RealmList
import io.realm.Sort
import kotlinx.android.synthetic.main.fragment_route.*

class FragmentRoute : Fragment(), OnMapReadyCallback, View.OnClickListener {

    private lateinit var mMap: GoogleMap
    private var flagRoute = false
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location

    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private var locationUpdateState = false
    private var draw = false
    private var resultTime: Long = 0L
    private var startTime: Long = 0L
    private var stopTime: Long = 0L
    private val realm: Realm = Realm.getDefaultInstance()
    private var countIds = 1
    private var txtName = ""

    private var listPoint: MutableList<LatLng> = arrayListOf()

    companion object {
        fun getInstance() : FragmentRoute {
            return FragmentRoute()
        }

        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val REQUEST_CHECK_SETTINGS = 2
    }

    override fun onCreateView(inflater: LayoutInflater, viewGroup: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_route, viewGroup, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_startEnd.setOnClickListener(this)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map_route) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)

                lastLocation = p0!!.lastLocation

                if (draw) {
                    drawLine((lastLocation))
                    //Toast.makeText(activity!!, "DRAW", Toast.LENGTH_LONG).show()
                }
            }
        }

        createLocationRequest()
    }

    override fun onMapReady(googleMap: GoogleMap) {

        mMap = googleMap

        mMap.uiSettings.isZoomControlsEnabled = true

        setUpMap()
    }

    override fun onClick(p0: View?) {
        when(p0?.id) {
            R.id.btn_startEnd -> {
                if (!flagRoute) {
                    start()
                    //TODO SEVICE
                } else {
                    stopAndSave()
                    //TODO SERVICE
                }
            }
        }
    }

    private fun start() {

        startTime = System.currentTimeMillis()

        flagRoute = true
        showDialog("Start")

        draw = true
        Toast.makeText(activity!!, "Draw: $draw", Toast.LENGTH_LONG).show()
        print("draw: $draw")

        mMap.clear()
        listPoint.clear()
    }

    private fun stopAndSave() {
        flagRoute = false
        showDialog("Stop")

        draw = false
        print("draw: $draw")
        Toast.makeText(activity!!, "Draw: $draw", Toast.LENGTH_LONG).show()

        //ADD FINAL POINT MARKER
        mMap.addMarker(MarkerOptions().position(LatLng(listPoint[listPoint.size - 1].latitude, listPoint[listPoint.size - 1].longitude)))
            .setIcon(BitmapDescriptorFactory.fromResource(R.drawable.flag))


        print(listPoint)

        stopTime = System.currentTimeMillis()

        resultTime = stopTime - startTime

        showDialogSetName()

    }

    private fun saveInRealm() {

        var listAux = RealmList<String>()

        for (i in listPoint) {
            var stringAux = ""
            stringAux = "${i.latitude},${i.longitude}"
            listAux.add(stringAux)
        }

        realm.executeTransactionAsync {

            val name = txtName
            val distance = getDistance(listPoint)
            val id = it.where(Routes::class.java).sort("id", Sort.DESCENDING).findFirst()!!.id + 1
            it.copyToRealm(Routes.create(id, name, distance.toString(), resultTime.toString(), listAux))
            //countIds += 1



            //it.copyToRealmOrUpdate(route)
            //realm.createObject(Routes::class.java, route)
        }

    }

    private fun getDistance(listpoint: MutableList<LatLng>): Double {

        var distance = 0.0

        for (x in 0 until listpoint.size - 1) {

            var pointA = listPoint[x]
            var pointB = listPoint[x + 1]

            var auxResult = SphericalUtil.computeDistanceBetween(pointA, pointB)

            distance += auxResult
        }

        return distance
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                locationUpdateState = true
                startLocationUpdate()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    override fun onResume() {
        super.onResume()
        if (!locationUpdateState) {
            startLocationUpdate()
        }
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(activity!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        mMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(activity!!) { location ->

            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 15f))
            }
        }
    }

    private fun startLocationUpdate() {
        if (ActivityCompat.checkSelfPermission(activity!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        val client = LocationServices.getSettingsClient(activity!!)

        val task = client.checkLocationSettings(builder.build())

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                    exception.startResolutionForResult(activity!!, REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {

                }
            }
        }
    }

    private fun drawLine(location: Location) {

        listPoint.add(LatLng(location.latitude, location.longitude))

        var line = PolylineOptions()

        for (item in listPoint) {
            line.add(item)
        }



        mMap.addPolyline(line).isGeodesic = true


        //ADD FIRST POINT MARKER
        mMap.addMarker(MarkerOptions().position(LatLng(listPoint[0].latitude, listPoint[0].longitude)))
            .setIcon(BitmapDescriptorFactory.fromResource(R.drawable.init))
    }

    private fun showDialog(txt: String) {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.pop_conteo)
        val start = dialog.findViewById(R.id.txt_start) as TextView

        start.text = txt

        dialog.show()

        if (flagRoute) {
            Handler().postDelayed({
                dialog.dismiss()
            }, 1000)
        } else {
            Handler().postDelayed({
                dialog.dismiss()
            }, 1000)
        }
    }

    private fun showDialogSetName() {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.pop_name_route)
        val name = dialog.findViewById(R.id.et_name_route) as EditText
        val accept = dialog.findViewById(R.id.btn_accept_name) as Button

        dialog.show()

        accept.setOnClickListener {

            txtName = name.text.toString()
            dialog.dismiss()

            saveInRealm()
        }
    }
}