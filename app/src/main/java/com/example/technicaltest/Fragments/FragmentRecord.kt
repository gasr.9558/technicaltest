package com.example.technicaltest.Fragments

import android.opengl.Visibility
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.technicaltest.Adapters.AdapterRecords
import com.example.technicaltest.R
import com.example.technicaltest.models.Routes
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_records.*

class FragmentRecord : Fragment() {

    val realm = Realm.getDefaultInstance()
    private val listRoutes: ArrayList<Routes> = arrayListOf()

    companion object {
        fun getIntance(): FragmentRecord {
            return FragmentRecord()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_records, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //listRoutes.clear()
        val routes = realm.where(Routes::class.java).findAll()
        listRoutes.addAll(routes)

        rv_records.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)

        if (routes.size <= 0) {
            ly_witout_items.visibility = View.VISIBLE
            rv_records.visibility = View.GONE
        } else {
            ly_witout_items.visibility = View.GONE
            rv_records.visibility = View.VISIBLE

            rv_records.adapter = AdapterRecords(context!!, listRoutes)
        }
    }
}