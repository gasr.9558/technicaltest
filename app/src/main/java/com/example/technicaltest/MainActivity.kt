package com.example.technicaltest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.technicaltest.Fragments.FragmentRecord
import com.example.technicaltest.Fragments.FragmentRoute
import com.example.technicaltest.models.NavigationMenu
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        configureUI()
    }

    private fun configureUI() {
        navigationBar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        configureFragment(NavigationMenu.ROUTE)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId) {
            R.id.navigation_route -> {
                configureFragment(NavigationMenu.ROUTE)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_record -> {
                configureFragment(NavigationMenu.RECORDS)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun configureFragment(type: NavigationMenu) {
        val fragment = type.getFragment()
        val t = type.name
        txt_title.text = type.getTitle(this)
        showFragment(t, fragment)
    }

    private fun showFragment(tag: String, fragment: Fragment) {
        if (supportFragmentManager.findFragmentByTag(tag) != null)
            supportFragmentManager.beginTransaction().attach(fragment).commit()
        else
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment, tag).commit()
    }
}
