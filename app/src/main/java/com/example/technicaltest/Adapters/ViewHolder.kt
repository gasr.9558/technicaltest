package com.example.technicaltest.Adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

    interface OnitemClick {
        fun onClick(position: Int, view: View)
    }
}