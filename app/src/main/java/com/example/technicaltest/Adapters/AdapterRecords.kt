package com.example.technicaltest.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.technicaltest.Activities.DetailRouteActivity
import com.example.technicaltest.R
import com.example.technicaltest.Utils.inflateView
import com.example.technicaltest.models.Routes
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.item_record.view.*
import kotlin.math.roundToInt

class AdapterRecords(private val context: Context, private val items: List<Routes>): RecyclerView.Adapter<AdapterRecords.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflateView(R.layout.item_record, false))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val i = items[position]

        holder.itemView.txt_name_route.text = i.name

        val distance = i.distance.toDouble().roundToInt()

        holder.itemView.txt_distace.text = distance.toString()

        holder.itemView.item_route.setOnClickListener {
            var intent = Intent(context, DetailRouteActivity::class.java)
            intent.putExtra("id_route", i.id)
            context.startActivity(intent)
        }
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

}